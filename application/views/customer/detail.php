<!-- <script>
$(document).ready( function () {

	var id = `<?php if(isset($id)) echo $id ?>`;

	$.post( "<?= base_url("customer/ajax") ?>", 
	{ 
		id: id 
	})

	$.ajax({
		url: url,
		type: method,
		data: data
	})
	.done(function( data ) {
		var d = $.parseJSON(data)
		render_chart(data);
	});

	function render_chart(data_Child)
	{	
		var chart_config = {
			chart: {
				container: "#collapsable-example",
				animateOnInit: true,
				node: {
					collapsable: true
				},
				animation: {
					nodeAnimation: "easeOutBounce",
					nodeSpeed: 700,
					connectorsAnimation: "bounce",
					connectorsSpeed: 700
				}
			},
			nodeStructure: data_Child
		}
		const tree = new Treant(chart_config);
	};
});
</script>

<div class="chart Treant loaded" id="collapsable-example"></div> -->


<!DOCTYPE html>
<html lang="en">
<meta charset="utf-8">

<style type="text/css">
	body {
	    font: 10px sans-serif;
	}
	.linage {
	    fill: none;
	    stroke: #000;
	}
	.marriage {
	    fill: none;
	    stroke: black;
	}
	.marriageNode {
			background-color: black;
			border-radius: 50%;
	}
	.man {
	    background-color: lightblue;
			border-style: solid;
			border-width: 1px;
			box-sizing: border-box;
	}
	.woman {
			background-color: pink;
			border-style: solid;
			border-width: 1px;
			box-sizing: border-box;
	}
	.emphasis{
			font-style: italic;
	}
	p {
		padding:0;
		margin:0;
	}
	svg {
		border-style: solid;
		border-width: 1px;
	}
</style>

<script src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
<script src="https://d3js.org/d3.v4.min.js"></script>
<script src="dTree.min.js"></script>
<body>
		<h1>Tree</h1>
    <div id="graph"></div>

		<script>
      treeJson = d3.json("<?= base_url("customer/ajax") ?>", function(error, treeData) {
      	dTree.init(treeData,
					{
						target: "#graph",
						debug: true,
						hideMarriageNodes: true,
						marriageNodeSize: 5,
						height: 800,
						width: 1200,
						callbacks: {
							nodeClick: function(name, extra) {
								alert('Click: ' + name);
							},
							// nodeRightClick: function(nama, extra) {
							// 	alert('Right-click: ' + nama);
							// },
							textRenderer: function(nama, extra, textClass) {
								if (extra && extra.nickname)
									nama = nama + " (" + extra.nickname + ")";
								return "<p align='center' class='" + textClass + "'>" + nama + "</p>";
							},
							marriageClick: function(extra, id) {
								alert('Clicked marriage node' + id);
							},
							marriageRightClick: function(extra, id) {
								alert('Right-clicked marriage node' + id);
							},
						}
					});
    	});
    </script>
</body>