<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Add Data
            </div>
            <div class="card-body">
                <?php if(validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors() ?>
                    </div>
                <?php endif; ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="name">Nama</label>
                        <input type="text" name="name" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="side">Side</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="side" id="side" value="Left">
                                <label class="form-check-label" for="side">
                                    Left
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="side" id="side" value="Right">
                                <label class="form-check-label" for="side">
                                    Right
                                </label>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" name="alamat" class="form-control" id="alamat">
                    </div>
                    <div class="form-group">
                        <label for="parent_id">ID Parent</label>
                        <input type="text" name="parent_id" class="form-control" id="parent_id">
                    </div>
                    <button type="submit" name="add" class="btn btn-primary float-right">Add Data</button>
                </form>
            </div>
        </div>
    </div>
</div>