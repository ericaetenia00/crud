<div class="container">

    <div class="row mt-3">
        <div class="com-md-6">
        <a href="<?= base_url(); ?>customer/add" class="btn btn-primary">Add Data</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="com-md-6">
            <h3>Data Customer</h3>
                <table id="data-table" class="table table-striped">
                    <thead>
                       <tr scope="col">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Side</th>
                        <th>Alamat</th>
                        <th>Parent_id</th>
                        <!-- <th>Bonus Pairing</th> -->
                        <th></th>
                        <th></th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach( $customer as $cus ): ?>
                            <tr>
                                <td><?=  $cus['id']; ?></td>
                                <td><?=  $cus['name']; ?></td>
                                <td><?=  $cus['side']; ?></td> 
                                <td><?=  $cus['alamat']; ?></td> 
                                <td><?=  $cus['parent_id']; ?></td>
                                <td><a href="<?= base_url(); ?>customer/bonus/<?= $cus['id'];?>" class="btn btn-info float-right">Bonus</a></td>
                                <td><a href="<?= base_url(); ?>customer/detail/<?= $cus['id'];?>" class="btn btn-info float-right">detail</a></td>
                                <td><a href="<?= base_url(); ?>customer/update/<?= $cus['id'];?>" class="btn btn-success float-right">edit</a></td>
                                <td><a href="<?= base_url(); ?>customer/delete/<?= $cus['id'];?>" class="btn btn-danger float-right" onclick="return confirm('delete data?');">delete</a></td>      
                            </tr>
                        <?php endforeach; ?>
                        
                    </tbody>
                    <script>
                    $(document).ready(function(){
                        $('#data-table').DataTable();

                    });
                    </script>
                </table>
            </ul>
        </div>
    </div>
</div>
