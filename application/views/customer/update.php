<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Update Data
            </div>
            <div class="card-body">
                <form action="<?php echo base_url('customer')?>" method="post">
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Alamat</label>
                        <input type="text" name="keterangan" class="form-control" id="keterangan">
                    </div>
                    <button type="submit" name="update" class="btn btn-primary float-right">Update Data</button>
                </form>
            </div>
        </div>
    </div>
</div>