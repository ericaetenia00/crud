<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Update Data
            </div>
            <div class="card-body">
                <form action="" method="post">
                    <input type="hidden" name="id" value="<?= $m_customer_level['id'];?>">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="jumlah_pairing">Jumlah Pairing</label>
                        <input type="text" name="jumlah_pairing" class="form-control" id="jumlah_pairing">
                    </div>
                    <div class="form-group">
                        <label for="hadiah">Hadiah</label>
                        <input type="text" name="hadiah" class="form-control" id="hadiah">
                    </div>
                    <button type="submit" name="update" class="btn btn-primary float-right">Update Data</button>
                </form>
            </div>
        </div>
    </div>
</div>