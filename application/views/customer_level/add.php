<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Add Data
            </div>
            <div class="card-body">
                <?php if(validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors() ?>
                    </div>
                <?php endif; ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="jumlah_pairing">Jumlah Pairing</label>
                        <input type="text" name="jumlah_pairing" class="form-control" id="jumlah_pairing">
                    </div>
                    <div class="form-group">
                        <label for="hadiah">Hadiah</label>
                        <input type="text" name="hadiah" class="form-control" id="hadiah">
                    </div>
                    <button type="submit" name="add" class="btn btn-primary float-right">Add Data</button>
                </form>
            </div>
        </div>
    </div>
</div>