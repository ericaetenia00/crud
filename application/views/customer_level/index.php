<div class="container">

    <div class="row mt-3">
        <div class="com-md-6">
        <a href="<?= base_url(); ?>customer_level/add" class="btn btn-primary">Add Data</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="com-md-6">
            <h3>Data Customer</h3>
                <table id="data-table" class="table table-striped">
                    <thead>
                       <tr scope="col">
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Jumlah Pairing</th>
                        <th>Hadiah</th>
                        <th></th>
                        <th></th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach( $customer as $cus ): ?>
                            <tr>
                                <td><?=  $cus['id']; ?></td>
                                <td><?=  $cus['nama']; ?></td>
                                <td><?=  $cus['jumlah_pairing']; ?></td> 
                                <td><?=  $cus['hadiah']; ?></td> 
                                <td><a href="<?= base_url(); ?>customer_level/update/<?= $cus['id'];?>" class="btn btn-success float-right">edit</a></td>
                                <td><a href="<?= base_url(); ?>customer_level/delete/<?= $cus['id'];?>" class="btn btn-danger float-right" onclick="return confirm('delete data?');">delete</a></td>      
                            </tr>
                        <?php endforeach; ?>
                        
                    </tbody>
                    <script>
                    $(document).ready(function(){
                        $('#data-table').DataTable();

                    });
                    </script>
                </table>
            </ul>
        </div>
    </div>
</div>