<!doctype html>
<html lang="en">
<head>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="treant/vendor/raphael.js"></script>
  <script src="Treant.min.js"></script>
  <script src="treant/Treant.js"></script>
  <script src="treant/vendor/jquery.min.js"></script>
  <script src="treant/vendor/jquery.easing.js"></script>
  <link rel="stylesheet" href="treant/Treant.css">
  <link rel="stylesheet" href="treant/examples/collapsable/collapsable.css">

  <script src="https://cdn.jsdelivr.net/npm/d3-dtree@2.4.1/dist/dTree.min.js"></script>
  <title>Data Customer</title>
</head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Data Customer</a>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-link float-right " href="<?= base_url(); ?>m_customer/index">Customer Type</a>
                    <a class="nav-link float-right " href="<?= base_url(); ?>customer_level/index">Customer Level</a>
                    <a class="nav-link float-right " href="<?= base_url(); ?>customer/index">Customer</a>
                </div>
            </div>
        </div>
    </nav>