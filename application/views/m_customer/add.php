<div class="container">
    <div class="row mt-3">
        <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                Add Data
            </div>
            <div class="card-body">
                <?php if(validation_errors()) : ?>
                    <div class="alert alert-danger" role="alert">
                        <?= validation_errors() ?>
                    </div>
                <?php endif; ?>
                <form action="" method="post">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" name="nama" class="form-control" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="bonus_pairing">Bonus Pairing</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="bonus_pairing" id="bonus_pairing" value="50000">
                                <label class="form-check-label" for="bonus_pairing">
                                    50,000
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="bonus_pairing" id="bonus_pairing" value="100000">
                                <label class="form-check-label" for="bonus_pairing">
                                    100,000
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="bonus_pairing" id="bonus_pairing" value="150000">
                                <label class="form-check-label" for="bonus_pairing">
                                    150,000
                                </label>
                            </div>
                    </div>
                    <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" name="keterangan" class="form-control" id="keterangan">
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label>
                        <input type="text" name="status" class="form-control" id="status">
                    </div>
                    <button type="submit" name="add" class="btn btn-primary float-right">Add Data</button>
                </form>
            </div>
        </div>
    </div>
</div>