<?php

class Customer_model extends CI_Model
{
    public function getAllCustomer()
    {
        return $this->db->get('customer')->result_array();
    }
    public function addDataCustomer()
    {
        $data = [
            "name" => $this->input->post('name', true),
            "side" => $this->input->post('side', true),
            "alamat" => $this->input->post('alamat', true),
            "parent_id" => $this->input->post('parent_id', true)
        ];

        $this->db->insert('customer', $data);
    }

    public function deleteDataCustomer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('customer', ['id' => $id]);
    }

    public function getCustomerById($id)
    {
        return $this->db->get_where('customer', ['id' => $id])->row_array();
    }

    public function updateDataCustomer($id)
    {
        $data = [
            "name" => $this->input->post('name', true),
            "alamat" => $this->input->post('alamat', true),
            "parent_id" => $this->input->post('parent_id', true)
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('customer', $data);
    }

    public function getChild()
    {
        $this->db->select('*');
        $this->db->from('customer');
        // $this->db->where('parent_id', $id);
        // $this->db->order_by('id', 'asc');
        // $this->db->limit(3, 0);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function getNamaByID($id) {
        $this->db->select('name');
        $this->db->from('customer');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
          return $query->first_row()->name;
        else
          return 0;
    }

    public function getBonusbyId($id)
    {
        $this->db->select('bonus');
        $this->db->from('customer');
        $this->db->where('id', $id);

        return $this->db->get()->row('bonus');
    }

    public function leftCount($id)
    {
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('side', "Left");
        $this->db->where('parent_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }

    public function rightCount($id)
    {
        $this->db->select('*');
        $this->db->from('customer');
        $this->db->where('side', "Right");
        $this->db->where('parent_id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return 0;
        }
    }
    

}
