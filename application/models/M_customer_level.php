<?php

class M_customer_level extends CI_Model
{
    public function getAllCustomer()
    {
        return $this->db->get('m_customer_level')->result_array();
    }
    public function addDataCustomer()
    {
        $data = [
            "id" => $this->input->post('id', true),
            "nama" => $this->input->post('nama', true),
            "jumlah_pairing" => $this->input->post('jumlah_pairing', true),
            "hadiah" => $this->input->post('hadiah', true)
        ];

        $this->db->insert('m_customer_level', $data);
    }

    public function deleteDataCustomer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('m_customer_level', ['id' => $id]);
    }

    public function getCustomerById($id)
    {
        return $this->db->get_where('m_customer_level', ['id' => $id])->row_array();
    }

    public function updateDataCustomer()
    {
        $data = [
            "id" => $this->input->post('id', true),
            "nama" => $this->input->post('nama', true),
            "jumlah_pairing" => $this->input->post('jumlah_pairing', true),
            "hadiah" => $this->input->post('hadiah', true)
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('m_customer_level', $data);
    }

}