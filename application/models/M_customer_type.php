<?php

class M_customer_type extends CI_Model
{
    public function getAllCustomer()
    {
        return $this->db->get('m_customer_type')->result_array();
    }
    public function addDataCustomer()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "bonus_pairing" => $this->input->post('bonus_pairing', true),
            "keterangan" => $this->input->post('keterangan', true),
            "status" => $this->input->post('status', true)
        ];

        $this->db->insert('m_customer_type', $data);
    }

    public function deleteDataCustomer($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('m_customer_type', ['id' => $id]);
    }

    public function getCustomerById($id)
    {
        return $this->db->get_where('m_customer_type', ['id' => $id])->row_array();
    }

    public function updateDataCustomer()
    {
        $data = [
            "nama" => $this->input->post('nama', true),
            "bonus_pairing" => $this->input->post('bonus_pairing', true),
            "keterangan" => $this->input->post('keterangan', true),
            "status" => $this->input->post('status', true)
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('m_customer_type', $data);
    }

}