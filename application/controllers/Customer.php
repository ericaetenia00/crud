<?php

class Customer extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Customer_model');
        $this->load->library('form_validation');
        $this->load->library('session');  
    }

    public function index()
    {
        $this->load->model('Customer_model');
        $data['judul'] = 'Daftar Customer';

        $data['customer'] = $this->Customer_model->getAllCustomer();
        $this->load->view('templates/header', $data);
        $this->load->view('customer/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $data['judul'] = 'Form Add Customer Data';

        $this->form_validation->set_rules('name', 'Name', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('customer/add');
            $this->load->view('templates/footer');
        } else {
            $this->Customer_model->addDataCustomer();
            redirect('customer');
        }

    }

    public function delete($id)
    {
        $this->Customer_model->deleteDataCustomer($id);
        $this->session->set_flashdata('flash', 'Delete');
        redirect('customer');
    }

    public function update($id)
    {
        $data['judul'] = 'Form Update Customer Data';
        $data['customer'] = $this->Customer_model->getCustomerById($id);

        $this->form_validation->set_rules('name', 'Name', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('customer/update', $data);
            $this->load->view('templates/footer');
        } else {
            $this->Customer_model->updateDataCustomer();
            $this->session->set_flashdata('flash', 'Update');
            redirect('customer');
        }
    }

    public function buildTree(array $elements, $parentId = 0) {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
    
        return $branch;
    }

    public function ajax(){
		$this->load->model('customer_model');
        $id = $this->input->post('id');
        $childs = $this->Customer_model->getChild();
        
        // $new = array();
        // if(is_array($childs)){
        //     foreach ($childs as $a){
        //         $new[$a['parent_id']][] = $a;
        //     }
        // }
		$list = $this->buildtree($childs, 0);
	
		header("Content-Type: application/json");
		echo json_encode($list);
	}

    public function HitungBonusPairing($id){
        $this->load->model('customer_model');
        $bonus = $this->Customer_model->getBonusbyId($id);
        $right = $this->Customer_model->rightCount($id);
        $left = $this->Customer_model->leftCount($id);
        $r = count($right);
        $l = count($left);
        if($r > $l)
        {
            $total = $l * $bonus;
        }
        else{
            $total = $r * $bonus;
        }
        return $total;
    }

    public function bonus($id)
    {
        $data['bonus'] = $this->HitungBonusPairing($id);
        $data['customer'] = $this->Customer_model->getCustomerById($id);
        $this->load->view('templates/header', $data);
        $this->load->view('customer/bonus', $data);
        $this->load->view('templates/footer');
    }

    public function detail($id)
    {
        $data['customer'] = $this->Customer_model->getCustomerById($id);
        $this->load->view('templates/header', $data);
        $this->load->view('customer/detail', $data);
        $this->load->view('templates/footer');

    }
}
