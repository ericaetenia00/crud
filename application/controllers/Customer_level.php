<?php

class Customer_level extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_customer_level');
        $this->load->library('form_validation');
        $this->load->library('session');  
    }

    public function index()
    {
        $this->load->model('M_customer_level');
        $data['judul'] = 'Daftar Customer';

        $data['customer'] = $this->M_customer_level->getAllCustomer();
        $this->load->view('templates/header', $data);
        $this->load->view('customer_level/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $data['judul'] = 'Form Add Customer Data';

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('customer_level/add');
            $this->load->view('templates/footer');
        } else {
            $this->M_customer_level->addDataCustomer();
            redirect('customer_level');
        }

    }

    public function delete($id)
    {
        $this->M_customer_level->deleteDataCustomer($id);
        $this->session->set_flashdata('flash', 'Delete');
        redirect('customer_level');
    }

    public function update($id)
    {
        $data['judul'] = 'Form Update Customer Data';
        $data['customer'] = $this->M_customer_level->getCustomerById($id);

        $this->form_validation->set_rules('name', 'Name', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('customer_level/update', $data);
            $this->load->view('templates/footer');
        } else {
            $this->M_customer_level->updateDataCustomer();
            $this->session->set_flashdata('flash', 'Update');
            redirect('customer');
        }
    }
}