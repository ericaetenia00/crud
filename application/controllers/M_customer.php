<?php

class M_customer extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_customer_type');
        $this->load->library('form_validation');
        $this->load->library('session');  
    }

    public function index()
    {
        $this->load->model('M_customer_type');
        $data['judul'] = 'Daftar Customer';

        $data['customer'] = $this->M_customer_type->getAllCustomer();
        $this->load->view('templates/header', $data);
        $this->load->view('m_customer/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $data['judul'] = 'Form Add Customer Data';

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('m_customer/add');
            $this->load->view('templates/footer');
        } else {
            $this->M_customer_type->addDataCustomer();
            redirect('m_customer');
        }

    }

    public function delete($id)
    {
        $this->M_customer_type->deleteDataCustomer($id);
        $this->session->set_flashdata('flash', 'Delete');
        redirect('m_customer');
    }

    public function update($id)
    {
        $data['judul'] = 'Form Update Customer Data';
        $data['customer'] = $this->M_customer_type->getCustomerById($id);

        $this->form_validation->set_rules('nama', 'Nama', 'required');
        
        if ( $this->form_validation->run() == FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('m_customer/update', $data);
            $this->load->view('templates/footer');
        } else {
            $this->M_customer_type->updateDataCustomer();
            $this->session->set_flashdata('flash', 'Update');
            redirect('m_customer');
        }
    }
}